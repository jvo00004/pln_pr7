import nltk
from nltk.tokenize import TreebankWordTokenizer
from nltk.chunk import tree2conlltags


file = open("nyt.txt")
text = file.read()

#Tokenizador
tokenizer = TreebankWordTokenizer()
#Etiquetas gramaticales
_POS_TAGGER = 'taggers/maxent_treebank_pos_tagger/english.pickle'
tagger = nltk.data.load(_POS_TAGGER)
#Texto
#text = 'John is studying at Stony Brook University in New York.'
#Tokenización de texto + etiquetación gramática
tags = tagger.tag(tokenizer.tokenize(text))
#Reconocimiento de entidades
_BINARY_NE_CHUNKER = 'chunkers/maxent_ne_chunker/english_ace_binary.pickle'
binary_ner = nltk.data.load(_BINARY_NE_CHUNKER)
ne_tree_binary = binary_ner.parse(tags)
#print(ne_tree_binary)
#Reconocimiento de entidades con el modelo multiclase
_MULTICLASS_NE_CHUNKER = 'chunkers/maxent_ne_chunker/english_ace_multiclass.pickle'
multiclass_ner = nltk.data.load(_MULTICLASS_NE_CHUNKER)
ne_tree_multiclass = multiclass_ner.parse(tags)
#print(ne_tree_multiclass)
#Salida en tripla
iob_tagged_multiclass = tree2conlltags(ne_tree_multiclass)
#print(iob_tagged_multiclass)


centinela = False
entidad = ""
string = ""
nombre_entidad = {}
nombre_apariciones = {}
categoria_nombre_apariciones = {}
for x in iob_tagged_multiclass:    
    #Si encuentra una entidad y no está procesando ninguna entidad, sigue buscando la siguiente palabra
    if x[2][0] == "B" and not centinela:
        string += x[0]
        entidad = x[2][2:]
        centinela = True
    #Si la entidad es compuesta, se compone
    elif x[2][0] == "I" and centinela:
        string += " " + x[0]
    #Si encuentra una entidad o un outside y está procesando una entidad, es que la entidad ha terminado
    elif x[2][0] == "B" or x[2][0] == 'O' and centinela:
        centinela = False
        nombre_entidad.update({string:entidad})
        if string in nombre_apariciones:
            nombre_apariciones.update({string:nombre_apariciones.get(string)+1})
        else:
            nombre_apariciones.update({string:1})
        string = ""
        entidad = ""

for x in nombre_apariciones:
    entidad = nombre_entidad.get(x)
    if entidad in categoria_nombre_apariciones:
        categoria_nombre_apariciones.get(entidad).update({x:nombre_apariciones.get(x)})
    else:
        categoria_nombre_apariciones.update({entidad:{x:1}})

